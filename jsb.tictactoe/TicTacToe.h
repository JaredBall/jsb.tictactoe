#pragma once

#include <iostream>
using namespace std;

class TicTacToe
{
private:
	static const int kBoardLength = 3;
	static const int kBoardSpaces = kBoardLength * kBoardLength;
	static const char kPlayer1 = 'X';
	static const char kPlayer2 = 'O';
	static const char kNone = ' ';

	char m_board[kBoardSpaces];
	int m_numTurns;
	char m_playerTurn;
	char m_winner;

	/// <summary>
	/// Checks if a player has won the game.
	/// </summary>
	/// <returns>True if a winner is detected.</returns>
	const bool DetectWinner();

	/// <summary>
	/// Checks if a player has won by going through the center.
	/// </summary>
	/// <returns>True if a winner is detected.</returns>
	const bool CheckCenter();

	/// <summary>
	/// Checks if the top or bottom row is marked by the same player.
	/// Start position must be the top-left or top-right.
	/// </summary>
	/// <param name="startPosition">0 or 8</param>
	/// <returns>True if a winner is detected.</returns>
	const bool CheckHorizontal(const int startPosition);

	/// <summary>
	/// Checks if the left or right column is marked by the same player.
	/// Start position must be the top-left or top-right.
	/// </summary>
	/// <param name="startPosition">0 or 8</param>
	/// <returns>True if a winner is detected.</returns>
	const bool CheckVertical(const int startPosition);

	/// <summary>
	/// Ends the current player's turn.
	/// </summary>
	void EndTurn();

public:
	/// <summary>
	/// Constructs a new TicTacToe game.
	/// </summary>
	TicTacToe();

	/// <summary>
	/// Returns an 'X' or an 'O', depending on which player's turn it is.
	/// </summary>
	/// <returns>char representing the current player.</returns>
	const char GetPlayerTurn() { return m_playerTurn; }

	/// <summary>
	/// Outputs a visual of the board to the console.
	/// </summary>
	const void DisplayBoard();

	/// <summary>
	/// Returns true if the game is over (winner or tie), otherwise false if the game is still being played.
	/// </summary>
	/// <returns>True if the game is over, false otherwise.</returns>
	const bool IsOver();
	
	/// <summary>
	/// Checks if the given position is open.
	/// </summary>
	/// <param name="position">1 - 9</param>
	/// <returns>True if the given position is valid, false otherwise.</returns>
	const bool IsValidMove(const int position);

	/// <summary>
	/// Places the current players character in the specified position on the board.
	/// </summary>
	/// <param name="position">1 - 9</param>
	void Move(const int position);

	/// <summary>
	/// Displays a message stating who the winning player is, or if the game ended in a tie.
	/// </summary>
	const void DisplayResult();
};
