#include "TicTacToe.h"

TicTacToe::TicTacToe()
{
	for (int i = 0; i < kBoardSpaces; i++)
	{
		m_board[i] = kNone;
	}
	m_numTurns = 0;
	m_playerTurn = kPlayer1;
	m_winner = kNone;
}

const bool TicTacToe::CheckCenter()
{
	bool winnerDetected = false;
	int startPosition = 4; // Middle
	char player = m_board[startPosition];

	if (player != kNone)
	{
		for (int i = 1; i < 5; i++)
		{
			if (m_board[startPosition - i] == player && m_board[startPosition + i] == player)
			{
				winnerDetected = true;
				break;
			}
		}
	}

	return winnerDetected;
}

const bool TicTacToe::CheckHorizontal(const int startPosition)
{
	int horizontalIncrement = 1;
	if (startPosition == 8)
		horizontalIncrement *= -1;
	bool winnerDetected = false;
	char player = m_board[startPosition];

	if (player != kNone)
	{
		int checkPosition = startPosition + horizontalIncrement;
		if (m_board[checkPosition] == player)
		{
			checkPosition += horizontalIncrement;
			if (m_board[checkPosition] == player)
				winnerDetected = true;
		}
	}

	return winnerDetected;
}

const bool TicTacToe::CheckVertical(const int startPosition)
{
	int verticalIncrement = 3;
	if (startPosition == 8)
		verticalIncrement *= -1;
	bool winnerDetected = false;
	char player = m_board[startPosition];

	if (player != kNone)
	{
		int checkPosition = startPosition + verticalIncrement;
		if (m_board[checkPosition] == player)
		{
			checkPosition += verticalIncrement;
			if (m_board[checkPosition] == player)
				winnerDetected = true;
		}
	}

	return winnerDetected;
}

const bool TicTacToe::DetectWinner()
{
	bool winnerDetected = CheckCenter();

	if (!winnerDetected)
		winnerDetected = CheckHorizontal(0);
	if (!winnerDetected)
		winnerDetected = CheckHorizontal(8);
	if (!winnerDetected)
		winnerDetected = CheckVertical(0);
	if (!winnerDetected)
		winnerDetected = CheckVertical(8);

	return winnerDetected;
}

void TicTacToe::EndTurn()
{
	if (m_playerTurn == kPlayer1)
		m_playerTurn = kPlayer2;
	else
		m_playerTurn = kPlayer1;
	m_numTurns++;
}

const void TicTacToe::DisplayBoard()
{
	int index = 0;
	while (index < kBoardSpaces)
	{
		cout << ' ' << m_board[index] << ' ';
		index++;
		if (index % kBoardLength == 0)
		{
			if (index < kBoardSpaces)
				cout << "\n-----------\n";
			else
				cout << endl;
		}
		else
			cout << '|';
	}
	cout << endl;
}

const bool TicTacToe::IsOver()
{
	return m_winner != kNone || m_numTurns >= kBoardSpaces;
}

const bool TicTacToe::IsValidMove(const int position)
{
	bool valid = false;
	if (!IsOver() && position > 0 && position <= kBoardSpaces)
	{

		char occupant = m_board[position - 1];
		if (occupant == kNone)
			valid = true;
	}
	return valid;
}

void TicTacToe::Move(const int position)
{
	m_board[position - 1] = m_playerTurn;
	bool hasWinner = DetectWinner();
	if (hasWinner)
		m_winner = m_playerTurn;
	EndTurn();
}

const void TicTacToe::DisplayResult()
{
	if (!IsOver())
		cout << "The game is still running.\n";
	else if (m_winner == kNone)
		cout << "The game is a draw.\n";
	else
		cout << "The winner is: Player " << m_winner << endl;
}
